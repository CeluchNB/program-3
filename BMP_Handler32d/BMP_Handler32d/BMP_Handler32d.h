/*
Noah Celuch
October 31, 2017
COMP 322, Object Oriented and Advanced Programming
File defines loading and saving of BMP file and 
declares structs for the headers
*/
#pragma once

#ifdef BMPEXPORTS
#define BMP_API __declspec(dllexport)
#else
#define BMP_API __declspec(dllimport)
#pragma comment(lib, "BMP_Handler32d.lib")
#endif

#include <iostream>
#include <fstream>

using namespace std;

class BMP_Handler {
public:
	static BMP_API unsigned char* loadBMP(const char*, int&, int&);

	static BMP_API void saveBMP(const char*, const unsigned char*, int, int);

	//struct for file header
	struct FileHeader {
		unsigned short type;
		unsigned int fileSize;
		short reserved1;
		short reserved2;
		int fileOffset;
	};

	//struct for info header
	struct DIBHeader {
		unsigned int size;
		int width;
		int height;
		unsigned short planes;
		short bitCount;
		int compression;
		int sizeImage;
		int xPixPerMeter;
		int yPixPerMeter;
		int colorUsed;
		int colorImportant;
	};
};