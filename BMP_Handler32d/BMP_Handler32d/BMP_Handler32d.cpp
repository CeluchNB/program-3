/*
Noah Celuch
October 31, 2017
COMP 322, Object Oriented and Advanced Programming
File implments loading and saving of BMP file based on the format
*/

#include "stdafx.h"
#include "BMP_Handler32d.h"

/*
For testing purposes, images are included
*/


BMP_API unsigned char* BMP_Handler::loadBMP(const char* filename, int& width, int& height) {
	ifstream fin(filename, ios::binary);
	FileHeader fileHeader;
	DIBHeader dibHeader;

	//read in both headers
	fin.read(reinterpret_cast<char*>(&fileHeader), sizeof(FileHeader));
	fin.read(reinterpret_cast<char*>(&dibHeader), sizeof(DIBHeader));

	//set width and height
	width = dibHeader.width;
	height = dibHeader.height;

	//calculate the total size
	int size = width*height * 3 + (width % 4)*height;
	
	//allocate space for rgb values
	unsigned char* values = new unsigned char[size];

	//write in each char worth of rgb value
	for (int i = 0; i < size; i++) {
		fin.read(reinterpret_cast<char*>(&values[i]), sizeof(char));
	}

	fin.close();
	return values;
}

BMP_API void BMP_Handler::saveBMP(const char* filename, const unsigned char* rgb, int width, int height) {
	ofstream fout(filename, ios::binary);
	
	FileHeader fileHeader;
	DIBHeader dibHeader;

	int padding = (width%4)*height;

	//set all values of first header
	fileHeader.type = 19778;
	fileHeader.fileSize = width*height * 3 + 54 + padding;
	fileHeader.reserved1 = 0;
	fileHeader.reserved2 = 0;
	//I decided to keep the file offset hardcoded
	//because it will always be 54 for this file type
	fileHeader.fileOffset = 54;
	
	//set all values of the info header
	dibHeader.size = 40;
	dibHeader.width = width;
	dibHeader.height = height;
	dibHeader.planes = 1;
	dibHeader.bitCount = 24;
	dibHeader.compression = 0;
	dibHeader.sizeImage = width*height*3 + padding;
	dibHeader.xPixPerMeter = 0;
	dibHeader.yPixPerMeter = 0;
	dibHeader.colorUsed = 0;
	dibHeader.colorImportant = 0;

	//write file header and info header to file
	fout.write(reinterpret_cast<char*>(&fileHeader), sizeof(FileHeader));
	fout.write(reinterpret_cast<char*>(&dibHeader), sizeof(DIBHeader));

	//assign rgb values to temporary array
	unsigned char* temp = new unsigned char[dibHeader.sizeImage];
	for (int i = 0; i < dibHeader.sizeImage; i++) {
		temp[i] = rgb[i];
	}

	//write in each rgb values
	for (int i = 0; i < dibHeader.sizeImage; i++) {
		fout.write(reinterpret_cast<char*>(&temp[i]), sizeof(char));
	}

	//close and delete dynamically allocated memory
	fout.close();
	delete [] temp;
}
